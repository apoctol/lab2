﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лабораторная
{
    class RefPars
    {
        public static List<string> id = new List<string>();
        public static List<string> name = new List<string>();
        public static List<string> discr = new List<string>();
        public static List<string> sourse = new List<string>();
        public static List<string> obj = new List<string>();
        public static List<string> KF = new List<string>();
        public static List<string> IF = new List<string>();
        public static List<string> AF = new List<string>();

        public static void Pars()
        {
            bool a = false;
            Refresh.Download(a);
            string ExcelfileName = @"C:\\Temp\test.xlsx"; //имя файла
            var objExcel = new Microsoft.Office.Interop.Excel.Application();
            var objWorkbook = objExcel.Workbooks.Open(ExcelfileName); //открываем Excel файл
            var objWorksheet = objWorkbook.Sheets[1]; //первый лист в файле
            //var objWorksheet = objWorkbook.Sheets["Лист1"]; //точное имя листа
            int iLastRow = objWorksheet.Cells[objWorksheet.Rows.Count, "A"].End[Microsoft.Office.Interop.Excel.XlDirection.xlUp].Row;  //последняя заполненная строка в столбце А
            var arrDataI = (object[,])objWorksheet.Range["A3:A" + iLastRow].Value; //берём данные со 3-й строки, если нужно с 1-й, то замените A2 на A1
            var arrDataN = (object[,])objWorksheet.Range["B3:B" + iLastRow].Value;
            var arrDataD = (object[,])objWorksheet.Range["C3:C" + iLastRow].Value;
            var arrDataS = (object[,])objWorksheet.Range["D3:D" + iLastRow].Value;
            var arrDataO = (object[,])objWorksheet.Range["E3:E" + iLastRow].Value;
            var arrDataKF = (object[,])objWorksheet.Range["F3:F" + iLastRow].Value;
            var arrDataIF = (object[,])objWorksheet.Range["G3:G" + iLastRow].Value;
            var arrDataAF = (object[,])objWorksheet.Range["H3:H" + iLastRow].Value;

            for (int i = 1; i <= arrDataI.GetUpperBound(0); i++)
            {
                id.Add(arrDataI[i, 1].ToString());
                name.Add(arrDataN[i, 1].ToString());
                discr.Add(arrDataD[i, 1].ToString());
                sourse.Add(arrDataS[i, 1].ToString());
                obj.Add(arrDataO[i, 1].ToString());
                if (int.Parse(arrDataKF[i, 1].ToString()) == 1)
                {
                    KF.Add("Да");
                }
                else
                {
                    KF.Add("Нет");
                }
                if (int.Parse(arrDataIF[i, 1].ToString()) == 1)
                {
                    IF.Add("Да");
                }
                else
                {
                    IF.Add("Нет");
                }
                if (int.Parse(arrDataAF[i, 1].ToString()) == 1)
                {
                    AF.Add("Да");
                }
                else
                {
                    AF.Add("Нет");
                }
                //checkedListBox1.Items.Add(arrDataI[i, 1].ToString());
            }
            objWorkbook.Close(true);//закрываем файл и сохраняем изменнения, если не сохранять, то false                
            objExcel.Quit(); //закрываем Excel
        }
    }
}
