﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Excel = Microsoft.Office.Interop.Excel;

namespace Лабораторная
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int change = 0;
        public MainWindow()
        {
            InitializeComponent();
            Pars.Collect();
            foreach (var item in Pars.id)
            {
                checkedListBox1.Items.Add(item);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
         {
            ID.Text = Pars.id[change-1];
            name.Text = Pars.name[change-1];
            disc.Text = Pars.discr[change-1];
            Source.Text = Pars.sourse[change-1];
            Object.Text = Pars.obj[change-1];
            KonfFail.Text = Pars.KF[change-1];
            IntFail.Text = Pars.IF[change-1];
            AcsFail.Text = Pars.AF[change-1];
        }

        private void CheckedListBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            change = int.Parse(comboBox.SelectedValue.ToString());
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Look taskWindow = new Look();
            taskWindow.Show();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            //Refresh.Ref();
            Refresh.Work();
            Close();
        }
    }
}
