﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Лабораторная
{
    /// <summary>
    /// Логика взаимодействия для Look.xaml
    /// </summary>
    public partial class Look : Window
    {
        static int BNomber = 1;
        //List<string>[] list = LookWork.Lookup(BNomber);
        public Look()
        {
                InitializeComponent();
                List<string>[] list = LookWork.Lookup(BNomber);
                List<string> ID = list[0];
                List<string> Name = list[1];
                ID_1.Text = ID[0];
                ID_2.Text = ID[1];
                ID_3.Text = ID[2];
                ID_4.Text = ID[3];
                ID_5.Text = ID[4];
                ID_6.Text = ID[5];
                ID_7.Text = ID[6];
                ID_8.Text = ID[7];
                ID_9.Text = ID[8];
                ID_10.Text = ID[9];
                ID_11.Text = ID[10];
                ID_12.Text = ID[11];
                ID_13.Text = ID[12];
                ID_14.Text = ID[13];
                ID_15.Text = ID[14];
                Name_1.Text = Name[0];
                Name_2.Text = Name[1];
                Name_3.Text = Name[2];
                Name_4.Text = Name[3];
                Name_5.Text = Name[4];
                Name_6.Text = Name[5];
                Name_7.Text = Name[6];
                Name_8.Text = Name[7];
                Name_9.Text = Name[8];
                Name_10.Text = Name[9];
                Name_11.Text = Name[10];
                Name_12.Text = Name[11];
                Name_13.Text = Name[12];
                Name_14.Text = Name[13];
                Name_15.Text = Name[14];
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BNomber++;
            List<string>[] list = LookWork.Lookup(BNomber);
            List<string> ID = list[0];
            List<string> Name = list[1];
            ID_1.Text = ID[0];
            ID_2.Text = ID[1];
            ID_3.Text = ID[2];
            ID_4.Text = ID[3];
            ID_5.Text = ID[4];
            ID_6.Text = ID[5];
            ID_7.Text = ID[6];
            ID_8.Text = ID[7];
            ID_9.Text = ID[8];
            ID_10.Text = ID[9];
            ID_11.Text = ID[10];
            ID_12.Text = ID[11];
            ID_13.Text = ID[12];
            ID_14.Text = ID[13];
            ID_15.Text = ID[14];
            Name_1.Text = Name[0];
            Name_2.Text = Name[1];
            Name_3.Text = Name[2];
            Name_4.Text = Name[3];
            Name_5.Text = Name[4];
            Name_6.Text = Name[5];
            Name_7.Text = Name[6];
            Name_8.Text = Name[7];
            Name_9.Text = Name[8];
            Name_10.Text = Name[9];
            Name_11.Text = Name[10];
            Name_12.Text = Name[11];
            Name_13.Text = Name[12];
            Name_14.Text = Name[13];
            Name_15.Text = Name[14];
            //Как второй вариант
            //Close();
            //Look taskWindow = new Look();
            //taskWindow.Show();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            BNomber--;
            if (BNomber == 0)
            {
                Close();
                BNomber = 1;
            }
            else
            {
                List<string>[] list = LookWork.Lookup(BNomber);
                List<string> ID = list[0];
                List<string> Name = list[1];
                ID_1.Text = ID[0];
                ID_2.Text = ID[1];
                ID_3.Text = ID[2];
                ID_4.Text = ID[3];
                ID_5.Text = ID[4];
                ID_6.Text = ID[5];
                ID_7.Text = ID[6];
                ID_8.Text = ID[7];
                ID_9.Text = ID[8];
                ID_10.Text = ID[9];
                ID_11.Text = ID[10];
                ID_12.Text = ID[11];
                ID_13.Text = ID[12];
                ID_14.Text = ID[13];
                ID_15.Text = ID[14];
                Name_1.Text = Name[0];
                Name_2.Text = Name[1];
                Name_3.Text = Name[2];
                Name_4.Text = Name[3];
                Name_5.Text = Name[4];
                Name_6.Text = Name[5];
                Name_7.Text = Name[6];
                Name_8.Text = Name[7];
                Name_9.Text = Name[8];
                Name_10.Text = Name[9];
                Name_11.Text = Name[10];
                Name_12.Text = Name[11];
                Name_13.Text = Name[12];
                Name_14.Text = Name[13];
                Name_15.Text = Name[14];
            }
        }
    }
}
