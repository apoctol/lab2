﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лабораторная
{
    class LookWork
    {
        public static string[] Looker (int x)
        {
                string[] s = new string[2];
                if (Pars.id[x] != null)
                {
                    s[0] = Pars.id[x];
                    s[1] = Pars.name[x];
                }
                else
                {
                    s[0] = null;
                    s[1] = null;
                }
                return s;
                       
        }
        public static List<string>[] Lookup(int n)
        {
            List<string>[] insert = new List<string>[2];
            insert[0] = new List<string>();
            insert[1] = new List<string>();
            for (int i = 1; i <= 15; i++)
            {
                string[] s = Looker((i-1)+15*(n-1));
                insert[0].Add(s[0]);
                insert[1].Add(s[1]);
            }
            return insert;
        }
    }
}
